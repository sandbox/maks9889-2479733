(function ($) {
	Drupal.behaviors.field_quick_label_editor = {
			attach: function (context, settings) {
				function update_size(for_all) {
					// set input size by length
					var length_val_max = 70;
					var length_val_min = 8;
					var elts = $('.field-edit-label');
					for(var i = 0; i < elts.length; i++) {
						var el = elts[i];
						
						var current_length = el.value.length;
						var length_val_loc = Math.max(length_val_min, Math.min(length_val_max, current_length + 1));
						if (for_all || el.readOnly == false && length_val_loc != el.size) {
							el.size = length_val_loc;
						}
					}
				}
				
				update_size(true);
				setInterval(update_size, 500);
				
				$('.field-edit-label').bind('mousedown', function(){
					if($(this).attr("readonly"))
					{
						return false;
					}
				});
				$('.field-edit-label').bind('focus', function(){
					if($(this).attr("readonly"))
					{
						$(this).blur();
					}
				});
				$('.field-edit-label').bind('dblclick', function(){
					$(this).removeAttr('readonly');
					if ($(".content-label-editor.messages").size()){
					} else {
						var comment = Drupal.t('Changes made in this table will not be saved until the form is submitted.');
						jQuery('<div class="content-label-editor messages warning">'+comment+'</div>').fadeIn('slow').insertBefore('#field-overview');
					}
				});
			}
	};

})(jQuery);